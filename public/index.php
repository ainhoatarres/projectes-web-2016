<?php

require  "../vendor/autoload.php";
require "../src/App/Routes.php";


$app = new Silex\Application();
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../src/App/Views',
));

$routes = new \App\Routes();

$routes->getRoutes($app);

$app->run();