<?php

namespace App;

use \Silex\Application;


class Routes {

    public function getRoutes(Application $app) {

        $app->mount('/index', new Controllers\IndexController());

        return $app;
    }

}

