<?php

namespace App\Controllers;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;

class IndexController implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        // TODO: Implement connect() method.

        $controllers = $app['controllers_factory'];

        $controllers->get('/', array($this, 'index'));
        $controllers->post('/', array($this, 'indexFormulari'));


        return $controllers;
    }

    public function index (Application $app) {
        return $app['twig']->render('index.twig');
    }

    public function indexFormulari (Application $app, Request $request) {
        $nom_usuari = $request->get('nom');

        return $app['twig']->render('index.twig', array('nom_usuari'=>$nom_usuari));

    }

}